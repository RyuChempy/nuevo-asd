using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct Imput
{
    public float hmovement;
    public float vmovement;

    public float verticalMouse;
    public float horizontalMouse;

    public bool dash;
    public bool jump;

    public void getInput()
    {
        hmovement = Input.GetAxis("Horizontal");
        vmovement = Input.GetAxis("Vertical");

        verticalMouse = Input.GetAxis("Mouse Y");
        horizontalMouse = Input.GetAxis("Mouse X");

        dash = Input.GetButton("Dash");
        jump = Input.GetButtonDown("Jump");
    }



}

